#!/usr/bin/env python
# encoding: utf-8

#######################################################################
#                            Network class                            #
#######################################################################

class Network(object):

	"""turn network into dict data structure, accessable network object"""
	
	
	def __init__(self, inputFilePath):
		"""inputFile

		:inputFilePath: @todo

		"""
		self._inputFilePath = inputFilePath
		self.network_dict=dict()
		self._getNetworkDict()
	def get2Targets(self, line):
		"""get 2 targets from string or one line

		:line: @todo
		:returns: @todo

		"""
		return line.split("|")
		pass
	def _getNetworkDict(self):
		"""generate the interaction diction for every component
		:returns: @todo

		"""
		with open(self._inputFilePath) as inputFile:
			for line in inputFile:
				line=line.strip()
				if line=="" or line.startswith("#"):
					continue
				
				self._pushData(self.get2Targets(line))
				print line
		#print self.network_dict
		pass

	def _pushData(self, targets):
		"""push data to dict

		:item: @todo
		:returns: @todo

		"""
		if targets[0] in self.network_dict.keys():
			if not targets[1] in self.network_dict[targets[0]].split("|"):
				self.network_dict[targets[0]]=self.network_dict[targets[0]]+"|"+targets[1]
		else:
			self.network_dict.setdefault(targets[0],targets[1])

		if targets[1] in self.network_dict.keys():
			if not targets[0] in self.network_dict[targets[1]].split("|"):
				self.network_dict[targets[1]]=self.network_dict[targets[1]]+"|"+targets[0]
		else:
			self.network_dict.setdefault(targets[1],targets[0])

		pass
	def _getComponentSet(self ):
		"""get all the component from network_dict, return the component set

		:returns: @todo

		"""
		component_set=self.network_dict.keys()
		for key in self.network_dict.keys():
			component_set=set.union(component_set,set(self.network_dict[key].split("|")))
		return component_set
		
		pass
	def _getSeedSet(self, seedFilePath):
		"""get seed set from seed file,
		returns the seed set

		:seedFilePath: @todo
		:returns: @todo

		"""
		seed_set=set()
		with open(seedFilePath) as seedFile:
			for line in seedFile:
				line=line.strip()
				if line=="" or line[0]=="#":
					continue
				seed_set.add(self.getSeed(line))
				pass	
		return seed_set
		pass
	def getSeed(self, line):
		"""get seed from line

		:line: @todo
		:returns: @todo

		"""
		return line
		pass
	def _getSubnetwork(self,seedFilePath, degree=1):
		"""get Subnetwork for seed

		:seedFilePath: @todo
		:degree: @todo
		:returns: @todo

		"""
		subnetwork_set=set()
		# get seeds
		seed_set=self._getSeedSet(seedFilePath)
		# seed old
		oldSeed_set=set(seed_set)
		newSeed_set=set(seed_set)

		while(degree>0):
			# update Subnetwork set
			for seed in seed_set:
				if seed in self.network_dict.keys():# search from network
					newSeed_set.add(seed)# add new element
					for target in self.network_dict[seed].split("|"):
						newSeed_set.add(target)#add new element
						if seed < target: # avoid duplication
							subnetwork_set.add(seed+"|"+target)
						else:
							subnetwork_set.add(target+"|"+seed)
			# update seed set
			seed_set=newSeed_set-oldSeed_set
			oldSeed_set=set(newSeed_set)

			degree=degree-1
			pass
		return subnetwork_set
		pass
	def _printSubnetwork(self, subnetwork_set):
		"""@todo: Docstring for _printSubnetwork.

		:subnetwork_set: @todo
		:returns: @todo

		"""
		for item in subnetwork_set:
			print "\t".join(item.split("|"))
		pass
	
def main():
	"""@todo: Docstring for main.
	:returns: @todo

	"""
	test=Network("test.txt")
	test._printSubnetwork(test._getSubnetwork("seed.test.txt",4))
	
	pass

if __name__ == '__main__':
	main()

