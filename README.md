# Network 
## Method for overriding 
- get2Targets
	get the list of 2 interating targets from PPI file
- getSeed
	get the seed from line of seed file
## Example
	# test file:
	# a|b
	# a|c
	# a|d
	# a|k
	# a|e
	# c|f
	# d|h
	# d|g
	# d|i
	# i|j
	# j|k
	# k|l
	# seed.test.txt:
	# a
	
	test=Network("test.txt")
	test._printSubnetwork(test._getSubnetwork("seed.test.txt",4))
	# output:
	# a|b
	# a|c
	# a|d
	# a|k
	# a|e
	# c|f
	# d|h
	# d|g
	# d|i
	# i|j
	# j|k
	# k|l
	# c	f
	# d	h
	# i	j
	# j	k
	# d	g
	# a	k
	# d	i
	# k	l
	# a	d
	# a	e
	# a	b
	# a	c

